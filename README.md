![The Long Night Is Coming](https://raw.githubusercontent.com/ZirtysPerzys/DRGL-X/master/12164.png)

# DRGL Blockchain & Icons 

First time users can now download this file for reasons of speed / convenience - use [this download of the DRGL blockchain...](https://github.com/ZirtysPerzys/DRGL-X/releases)

---------------------------
### To bootstrap blockchain:
Just extract the contents of the zip file, from the link above, into your .dragonglass folder- a 'hidden' folder in your user home base directory.  
(Access hidden files and folders by pressing "Alt + h" )

For Windows users the location will be C:\Users\USERNAME\AppData\Roaming\dragonglass
